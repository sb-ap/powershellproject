﻿function New-CustomChannel{
    [CmdletBinding()]
    param(
    [Parameter(Mandatory,ValueFromPipeline)]
    [string]$GroupID,
    [Parameter(Mandatory,ValueFromPipelineByPropertyName)]
    [string]$ChannelName
    )
    Begin {
    Write-Host "the code is starting"
    }
    Process {
    Write-Host $GroupID
    Write-Host $ChannelName
    }
    
    End {
    Write-Host "the code is almost ending"
    } 
}